const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require('path');
const webpack = require('webpack');


var isProd = process.env.NODE_ENV === 'production';
var cssDev = ['style-loader','css-loader','sass-loader'];
var cssProd =  ExtractTextPlugin.extract({
            fallback : 'style-loader',
            use : [
                'css-loader?minimize=false',
                'sass-loader',
            ],
            publicPath : '/dist/'
        });
var cssConfig = isProd ? cssProd : cssDev;

module.exports = {

    entry :{

        styleguide : './src/js/styleguide.js',
        detailpage : './src/js/detailpage.js',
        index : './src/js/index.js',
    },
    mode : 'development',
    output : {
        path : path.resolve(__dirname, 'dist'),
        filename : './js/[name].bundle.js'
    },
    module : {
        rules : [
            {
                test : /\.scss$/, 
                use :cssConfig
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            { 
                test: /\.(jpe?g|gif|png|svg)$/i, 
                use:  [
                   'file-loader?name=[name].[ext]&outputPath=images/&publicPath=images/',
                    //'image-webpack-loader'
                ]
            },

        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9003,
        open : true,
        stats : 'minimal',
        hot : false,
        index : 'index.html'
    },


    plugins: [
        new HtmlWebpackPlugin({
            title: 'index',
            filename: 'index.html',
            minify : {
                collapseWhitespace : false
            },
            hash : true,
            chunks : ['index'],
            template: './src/view/index.ejs'
        }),
        new HtmlWebpackPlugin({
            title: 'STYLE GUIDE',
            filename: 'styleguide.html',
            minify : {
                collapseWhitespace : false
            },
            hash : true,
            chunks : ['styleguide'],
            template: './src/view/styleguide.ejs'
        }),
        new HtmlWebpackPlugin({
            title: 'DETAIL PAGE',
            filename: 'detailpage.html',
            minify : {
                collapseWhitespace : false
            },
            hash : true,
            chunks : ['detailpage'],
            template: './src/view/detailpage.ejs'
        }),
        new ExtractTextPlugin({
            filename : "style/[name].css",
            disable : !isProd,
            allChunks : true
        }),
        new webpack.HotModuleReplacementPlugin(),//HMR GLOBAL
        new webpack.NamedModulesPlugin(),//
      ]
}